"""
This module represents the Consumer.

Computer Systems Architecture Course
Assignment 1
March 2021
"""

from threading import Thread
import time

class Consumer(Thread):
    """
    Class that represents a consumer.
    """

    def __init__(self, carts, marketplace, retry_wait_time, **kwargs):
        """
        Constructor.

        :type carts: List
        :param carts: a list of add and remove operations

        :type marketplace: Marketplace
        :param marketplace: a reference to the marketplace

        :type retry_wait_time: Time
        :param retry_wait_time: the number of seconds that a producer must wait
        until the Marketplace becomes available

        :type kwargs:
        :param kwargs: other arguments that are passed to the Thread's __init__()
        """
        Thread.__init__(self, **kwargs)
        self.carts = carts
        self.marketplace = marketplace
        self.retry_wait_time = retry_wait_time

    def run(self):
        for cart in self.carts:
            # create new cart
            cart_id = self.marketplace.new_cart()
            for operation in cart:
                # get type, product an quantity
                operation_type = operation['type']
                product = operation['product']
                quantity = operation['quantity']

                if operation_type == 'add':
                    # add all items in cart
                    while quantity > 0:
                        add_response = self.marketplace.add_to_cart(cart_id, product)
                        # if add to cart was succesfully, decrement quantity else wait
                        if add_response:
                            quantity = quantity - 1
                        else:
                            time.sleep(self.retry_wait_time)
                elif operation_type == 'remove':
                    # remove all items in cart
                    while quantity > 0:
                        remove_response = self.marketplace.remove_from_cart(cart_id, product)
                        # if remove from cart was succesfully, decrement quantity else wait
                        if remove_response:
                            quantity = quantity - 1
                        else:
                            time.sleep(self.retry_wait_time)

            # cart is complete, place order
            order = self.marketplace.place_order(cart_id)

            # print order from marketplace to sync threads
            for order_item in order:
                self.marketplace.print_cart(self.name, order_item)
