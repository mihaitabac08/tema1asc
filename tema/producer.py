"""
This module represents the Producer.

Computer Systems Architecture Course
Assignment 1
March 2021
"""

import time
from threading import Thread


class Producer(Thread):
    """
    Class that represents a producer.
    """

    def __init__(self, products, marketplace, republish_wait_time, **kwargs):
        """
        Constructor.

        @type products: List()
        @param products: a list of products that the producer will produce

        @type marketplace: Marketplace
        @param marketplace: a reference to the marketplace

        @type republish_wait_time: Time
        @param republish_wait_time: the number of seconds that a producer must
        wait until the marketplace becomes available

        @type kwargs:
        @param kwargs: other arguments that are passed to the Thread's __init__()
        """
        Thread.__init__(self, **kwargs)
        self.products = products
        self.marketplace = marketplace
        self.republish_wait_time = republish_wait_time
        self.producer_id = marketplace.register_producer()

    def run(self):
        while True:
            for product in self.products:
                product_ = product[0]
                quantity_ = product[1]
                wait_time_ = product[2]

                while quantity_ > 0:
                    publish_result = self.marketplace.publish(self.producer_id, product_)
                    while not publish_result:
                        time.sleep(wait_time_)
                        publish_result = self.marketplace.publish(self.producer_id, product_)
                    quantity_ = quantity_ - 1

            time.sleep(self.republish_wait_time)
