"""
This module represents the Marketplace.

Computer Systems Architecture Course
Assignment 1
March 2021
"""
import random
from threading import Semaphore, Lock


class Marketplace:
    """
    Class that represents the Marketplace. It's the central part of the implementation.
    The producers and consumers use its methods concurrently.
    """

    def __init__(self, queue_size_per_producer):
        """
        Constructor

        :type queue_size_per_producer: Int
        :param queue_size_per_producer: the maximum size of a queue associated with each producer
        """
        self.queue_size_per_producer = queue_size_per_producer
        self.sem_prod = {}
        self.products = {}
        self.carts = {}
        self.print_lock = Lock()

    def register_producer(self):
        """
        Returns an id for the producer that calls this.
        """
        randint = random.randint(0, 1000000)
        while True:
            # if id already exists generate another random
            producer_id = str(randint)
            if self.sem_prod.get(producer_id) is None:
                # create semaphore for producer
                self.sem_prod[producer_id] = Semaphore(self.queue_size_per_producer)
                return randint
            randint = random.randint(0, 1000000)

    def publish(self, producer_id, product):
        """
        Adds the product provided by the producer to the marketplace

        :type producer_id: String
        :param producer_id: producer id

        :type product: Product
        :param product: the Product that will be published in the Marketplace

        :returns True or False. If the caller receives False, it should wait and then try again.
        """
        producer_id_string = str(producer_id)
        if self.sem_prod.get(producer_id_string).acquire(blocking=False):
            product_name = product.name
            if self.products.get(product_name) is None:
                self.products[product_name] = []

            self.products[product_name].append({
                "producer_id": producer_id_string
            })
            return True
        return False

    def new_cart(self):
        """
        Creates a new cart for the consumer

        :returns an int representing the cart_id
        """
        randint = random.randint(0, 1000000)
        while True:
            # if id already exists generate another random
            cart_id = str(randint)
            if self.carts.get(cart_id) is None:
                self.carts[cart_id] = []
                return cart_id
            randint = random.randint(0, 1000000)

    def add_to_cart(self, cart_id, product):
        """
        Adds a product to the given cart. The method returns

        :type cart_id: Int
        :param cart_id: id cart

        :type product: Product
        :param product: the product to add to cart

        :returns True or False. If the caller receives False, it should wait and then try again
        """
        product_to_add = self.products[product.name]
        # if product does not exists return False
        if product_to_add is None or len(product_to_add) == 0:
            return False

        product_with_producer = self.products[product.name].pop()
        producer_id = product_with_producer.get("producer_id")

        producer_id = str(producer_id)
        cart_id_string = str(cart_id)

        self.sem_prod.get(producer_id).release()
        self.carts[cart_id_string].append({
            "product": product,
            "producer_id": producer_id
        })
        return True

    def remove_from_cart(self, cart_id, product):
        """
        Removes a product from cart.

        :type cart_id: Int
        :param cart_id: id cart

        :type product: Product
        :param product: the product to remove from cart
        """
        current_cart = self.carts[str(cart_id)]
        for product_in_cart in current_cart:
            product_name = product.name
            if product_in_cart["product"].name == product_name:
                if self.products.get(product_name) is None:
                    self.products[product_name] = []

                # put the product back to market
                producer_id = str(product_in_cart["producer_id"])
                self.products[product_name].append({
                    "producer_id": producer_id
                })

                # try to limit the producer queue
                self.sem_prod.get(producer_id).acquire(blocking=False)

                # remove product from current cart
                current_cart.remove(product_in_cart)
                return True
        return False
        # return True

    def place_order(self, cart_id):
        """
        Return a list with all the products in the cart.

        :type cart_id: Int
        :param cart_id: id cart
        """
        cart_ret = []
        cart_id_string = str(cart_id)
        current_cart = self.carts[cart_id_string]
        for product_in_cart in current_cart:
            cart_ret.append(product_in_cart["product"])
        return cart_ret

    def print_cart(self, consumer, product):
        """
        Print elements in cart thread safe

        :type consumer: String
        :param consumer: Consumer name

        :type product: String
        :param product: Product name
        """
        self.print_lock.acquire()
        print(consumer, "bought", product)
        self.print_lock.release()
