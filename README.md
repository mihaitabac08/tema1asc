Nume: TABAC MIHAI CRISTIAN
Grupă: 342C5

# Tema 1 ASC
-
1. Explicație pentru soluția aleasă:

Abordarea generala consta in crearea Semafoarelor pentru operatiile de modificare cosuri precum si de listarea de produse.
Pentru un control sporit asupra elementelor, am folosit Map-uri pentru :
* producatori - cheia fiind producer_id iar valoarea de tip <b>Semaphore</b>
* produse - cheia fiind numele produsului iar valoare o lista de producatori (id-ul producatorilor).
* cosuri - cheia fiind cart_id-ul unic iar valoarea un obiect cu 2 atribute: produsul, respectiv id-ul producatorului



Implementare
-

* Am creat pentru fiecare consumator cate un cos de cumparaturi la care am implementat operatiile de add si de remove. 
* La operatia de add se vor adauga toate itemele date ca parametru, iar la operatia de remove se vor sterge toate itemele. 
* Dupa ce toate operatiile au fost effectuate si cosul de cumparaturi este complet atunci plasam comanda.
* Am creat un semafor cu cate produse poate sa listeze maxim in marketplace fiecare producator si se adauga in lista de producatori id ul si semaforul fiecarui producer.
* La publish se face acquire pe semforul produsului respectiv, dupa se adauga in lista de produse din marketplace prosusul si id ul producatorului. In cazul in care nu se poate face acquire pe semaforul produsului se va intoarca False inseamnand ca producatorul are coada proprie plina de produse pe care poate sa le listeze
* Pentru adaugarea in cos, se verifica intai daca produsul exista in lista de produse din marketplace, iar daca exista se ia elementul, se adauga in cartul respectiv si se face release la producatorul care are produsul respective. In cazul in care exista produsul se returneaza True.
* Pentru stergerea din cos, se caute elementul in cosul respective, daca se gaseste se scoate din cart, se pune inapoi in lista de elemente din marketplace si se incearca un acquire pe semaforul producatorului respective(s ar putea ca producatorul sa aiba deja coada plina si de aceea doar se incearca)	
* A fost nevoie de implementarea unei functii de printare a cosului, si s-a folosit un lock pentru a putea scrie threade safe la consola


Resurse utilizate
-

* https://ocw.cs.pub.ro/courses/asc/laboratoare/02
* https://ocw.cs.pub.ro/courses/asc/laboratoare/03
* Stackoverflow

Git
-
1. Link către repo-ul de git
https://gitlab.com/mihaitabac08/tema1asc.git
